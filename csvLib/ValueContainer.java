/**
 * Small container that provides a values type, and it's string representation.
 */
public final class ValueContainer<T> {
    /** Represents the actual value */
    public T value;
    public ValueContainer (T value) {
        this.value = value;
    }

    @Override
    public String toString() {
        if (this.value instanceof Integer) return String.valueOf((int) this.value);
        if (this.value instanceof Double) return String.valueOf((double) this.value);
        return "\"" + this.value + "\"";
    }
}