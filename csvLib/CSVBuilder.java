import java.io.*;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * CSVBuilder builds a whole csv file from a table.
 * Holds table data and let you operate with them in the users file system.
 */
public final class CSVBuilder {

    /**
     * Total CSV file line count
     */
    private int totalLineCount;

    /**
     * CSV file in bytes
     */
    private String[] csvFile;

    /**
     * CSV file as table
     */
    private HashMap<Integer, ArrayList<ValueContainer>> table;

    /**
     * CSV file in fs
     */
    private File file;

    /**
     * Creates a new CSV file in users filesystem
     *
     * @param outAbsPath Output absolute path from filesystem
     * @return Successfully created: true | On failure: false
     */
    public static CSVBuilder createInFS(final String outAbsPath, final HashMap<Integer, ArrayList<ValueContainer>> table) {
        assert (outAbsPath != null);
        Path p = Paths.get(outAbsPath);
        try {
            CSVBuilder csvBuilder = new CSVBuilder();
            csvBuilder.table = table;

            csvBuilder.file = p.toFile();
            boolean fileBuildResult = true;
            if (!csvBuilder.file.exists()) fileBuildResult = csvBuilder.file.createNewFile();
            if (!fileBuildResult) {
                System.out.format(">> [CSVBuilder]: Failed to create file at \"%s\"\n", p);
                return null;
            }


            try {
                // write in file
                csvBuilder.updateCSVFileInFS();

                System.out.format(">> [CSVBuilder]: Created file \"%s\"\n", p);
                return csvBuilder;
            } catch (Exception e) {
                System.out.format(">> [CSVBuilder]: Failed to write in file \"%s\"\n", p);
                return null;
            }
        } catch (Exception e) {
            System.out.format(">> [CSVBuilder]: Failed to create file at \"%s\"\n", p);
            return null;
        }
    }

    /**
     * Adds or inserts a value into the table.
     * Also updates the CSV file.
     *
     * @param lIndex Line Index
     * @param index  Value index in row | if index == -1 append to end of line
     * @param value  Actual value
     * @param <T>    Value type
     * @return Result
     */
    public final <T> boolean add(final int lIndex, final int index, T value) {
        if (lIndex < 0) {
            System.out.format(">> [CSVBuilder]: Can't add or insert at negative index %d!\n", lIndex);
            return false;
        }

        // add or insert
        ArrayList<ValueContainer> line;
        if (this.table.get(lIndex) == null) { // lIndex-key not existing
            line = new ArrayList<>();
            line.add(new ValueContainer<>(value));
            this.table.put(lIndex, line);
        } else { // lIndex-key existing
            line = this.table.get(lIndex);
            if (index == -1) line.add(new ValueContainer<>(value)); // add on end of line
            else line.add(index, new ValueContainer<>(value)); // insert in line at index
        }

        this.updateCSVFileInFS();

        return true;
    }

    /**
     * Adds or overrides (if lIndex already existing as key) line at lIndex.
     *
     * @param lIndex Line index
     * @param line   Line
     * @return Result
     */
    public final boolean add(final int lIndex, final ArrayList<ValueContainer> line) {
        if (lIndex < 0) {
            System.out.format(">> [CSVBuilder]: Can't add or override at negative index %d!\n", lIndex);
            return false;
        }

        // add or override
        this.table.put(lIndex, line);

        this.updateCSVFileInFS();

        return true;
    }

    /**
     * Joins two tables by keys (line indices).
     *
     * @param table Actual table
     */
    public final void add(final HashMap<Integer, ArrayList<ValueContainer>> table) {
        HashMap<Integer, ArrayList<ValueContainer>> tmp = new HashMap<>(table);
        tmp.keySet().removeAll(this.table.keySet());
        this.table.putAll(tmp);

        this.updateCSVFileInFS();
    }

    /**
     * Sets the value at lIndex and value in line index to new value.
     *
     * @param lIndex Line index
     * @param index  Value index
     * @param value  New value
     * @param <T>    Value Type
     * @return Result
     */
    public final <T> boolean set(final int lIndex, final int index, T value) {
        if (lIndex < 0) {
            System.out.format(">> [CSVBuilder]: Can't set at negative index %d!\n", lIndex);
            return false;
        }

        if (index >= this.table.get(lIndex).size() || index < 0) {
            System.out.format(">> [CSVBuilder]: Out of bounds in line %d at index %d!\n", lIndex, index);
            return false;
        }

        this.remove(lIndex, index);
        this.add(lIndex, index, value);
        return true;
    }

    /**
     * Sets the line at lIndex to new line.
     *
     * @param lIndex Line index
     * @param line   New line
     * @return Result
     */
    public final boolean set(final int lIndex, ArrayList<ValueContainer> line) {
        if (lIndex < 0) {
            System.out.format(">> [CSVBuilder]: Can't set at negative index %d!\n", lIndex);
            return false;
        }

        this.remove(lIndex);
        this.add(lIndex, line);
        return true;
    }

    /**
     * Removes a value in a line.
     *
     * @param lIndex Line Index
     * @param index  Value index
     * @return Result
     */
    public final boolean remove(final int lIndex, final int index) {
        if (lIndex < 0) {
            System.out.format(">> [CSVBuilder]: Can't remove line at negative index %d!\n", lIndex);
            return false;
        }

        if (this.table.get(lIndex) == null) {
            System.out.format(">> [CSVBuilder]: Line %d doesn't exists!\n", lIndex);
            return false;
        }

        if (index >= this.table.get(lIndex).size() || index < 0) {
            System.out.format(">> [CSVBuilder]: Out of bounds in line %d at index %d!\n", lIndex, index);
            return false;
        }

        this.table.get(lIndex).remove(index);
        return true;
    }

    /**
     * Removes a line.
     *
     * @param lIndex Line Index
     * @return Result
     */
    public final boolean remove(final int lIndex) {
        if (lIndex < 0) {
            System.out.format(">> [CSVBuilder]: Can't remove line at negative index %d!\n", lIndex);
            return false;
        }

        if (this.table.get(lIndex) == null) {
            System.out.format(">> [CSVBuilder]: Line %d doesn't exists!\n", lIndex);
            return false;
        }

        this.table.remove(lIndex);
        return true;
    }

    /**
     * Updates CSV file in file system.
     */
    private void updateCSVFileInFS() {
        try {
            FileWriter fileWriter = new FileWriter(this.file);
            fileWriter.write(this.toString());
            fileWriter.close();
        } catch (IOException e) {
            System.out.format(">> [CSVBuilder]: Failed to write in file: \"%s\"\n", this.file.getPath());
        }
    }


    /**
     * Getting the total line count
     *
     * @return @{@link CSVBuilder#totalLineCount}
     */
    public final long getTotalLineCount() {
        return this.totalLineCount;
    }

    /**
     * Getting the final table
     *
     * @return @{@link CSVBuilder#table}
     */
    public final HashMap<Integer, ArrayList<ValueContainer>> getTable() {
        return this.table;
    }

    /**
     * Getting a value container
     *
     * @param lIndex line index
     * @param index  value container index in line
     * @return @{@link ValueContainer}
     */
    public final ValueContainer getValueContainer(final int lIndex, final int index) {
        return this.table.get(lIndex).get(index);
    }

    /**
     * Setting new value for container in table
     *
     * @param lIndex Line index
     * @param index  Value container index in line
     * @param value  Actual value
     * @param <T>    Value type
     */
    public <T> void setValueContainer(final int lIndex, final int index, T value) {
        this.table.get(lIndex).set(index, new ValueContainer(value));
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (Map.Entry<Integer, ArrayList<ValueContainer>> entry : table.entrySet()) {
            for (ValueContainer vC : entry.getValue()) {
                ValueContainer lastEntry = entry.getValue().get(entry.getValue().size() - 1);
                result.append(vC != lastEntry ? vC.toString() + "," : vC.toString());
            }
            result.append("\n");
        }
        return result.toString();
    }
}