import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * CSVParser parses a whole csv file to a table.
 * Holds table data and let you operate with them in java source code.
 */
public final class CSVParser {

    /**
     * ERROR status codes
     */
    public enum CHECK {SUCCESS, WRONG_TYPE, SYNTAX_ERROR}

    /**
     * ERROR check var
     */
    public CHECK C = CHECK.SUCCESS;
    /**
     * Total CSV file line count
     */
    private int totalLineCount;
    /**
     * CSV file in bytes
     */
    private String[] csvFile;
    /**
     * CSV file as table
     */
    private HashMap<Integer, ArrayList<ValueContainer>> table;

    /**
     * Loads an CSV file from own jar file.
     * This class must be in same jar file!
     *
     * @param inRelPath Input relative path from project source compile origin in own jar file
     * @return Successfully loaded: new CSVParser object | On failure: null
     */
    public static CSVParser loadFromJar(final String inRelPath) {
        assert (inRelPath != null);
        try {
            CSVParser csvParser = new CSVParser();

            InputStream is = csvParser.getClass().getResourceAsStream(inRelPath);
            byte[] allBytes = is.readAllBytes();
            is.close();

            StringBuilder bytes = new StringBuilder();
            for (byte b : allBytes)
                bytes.append((char) b);

            if (bytes.toString().contains(",,")) {
                csvParser.C = CHECK.SYNTAX_ERROR;
                System.out.format(">> [CSVParser]: Adjacent commas in file!\n");
                return null;
            }
            csvParser.csvFile = bytes.toString().split("\n");
            csvParser.totalLineCount = csvParser.csvFile.length;
            if (!csvParser.parseMap()) return null;

            System.out.format(">> [CSVParser]: Loaded file \"%s\"\n", inRelPath);
            csvParser.C = CHECK.SUCCESS;

            return csvParser;
        } catch (Exception e) {
            System.out.format(">> [CSVParser]: Failed to load path \"%s\"\n", inRelPath);
            return null;
        }
    }

    /**
     * Loads an CSV file from users filesystem.
     *
     * @param inAbsPath Input absolute path from filesystem
     * @return Successfully loaded: new CSVParser object | On failure: null
     */
    public static CSVParser loadFromFS(final String inAbsPath) {
        assert (inAbsPath != null);
        Path p = Paths.get(inAbsPath);
        try {
            CSVParser csvParser = new CSVParser();
            String bytes = new String(Files.readAllBytes(p));
            if (bytes.contains(",,")) {
                csvParser.C = CHECK.SYNTAX_ERROR;
                System.out.format(">> [CSVParser]: Adjacent commas in file!\n");
                return null;
            }
            csvParser.csvFile = bytes.split("\n");
            csvParser.totalLineCount = csvParser.csvFile.length;
            if (!csvParser.parseMap()) return null;

            System.out.format(">> [CSVParser]: Loaded file \"%s\"\n", p);
            csvParser.C = CHECK.SUCCESS;

            return csvParser;
        } catch (Exception e) {
            System.out.format(">> [CSVParser]: Failed to load path \"%s\"\n", p);
            return null;
        }
    }

    /**
     * Parses a whole csv line
     *
     * @param lIndex Line index
     * @return One line represented in a list of Integers, Doubles and Strings
     */
    private ArrayList<ValueContainer> parseLine(final int lIndex) {
        assert (lIndex >= 0 && lIndex < this.totalLineCount);

        ArrayList<ValueContainer> line = new ArrayList<>();
        String[] values = this.csvFile[lIndex].split(",");

        for (String v : values) {
            // filter if null, empty string or carriage return
            if (v == null || v.equals("") || v.toCharArray()[0] == 13) continue;

            //DEBUG
            //System.out.print(line.size() > 0 ? line.get(line.size() - 1).value + "," : "[Line Start]: "); // DEBUG!

            /* ------------------- PARSE STRING ------------------- */

            // String instance with "
            if (v.contains("\"")) {
                if (v.chars().filter(c -> c == '\"').count() >= 2) {
                    line.add(new ValueContainer<String>(v.substring(v.indexOf('\"') + 1, v.lastIndexOf('\"')))); // cropped string value
                    continue;
                } else { // Invalid value
                    this.C = CHECK.SYNTAX_ERROR;
                    System.out.format(">> [CSVParser]: Invalid char at line [%d], in value [%s], at index [%d]\n", lIndex, v, v.indexOf('\"'));
                    return null;
                }

            }

            v = v.replaceAll(" ", ""); // remove spaces

            if (v.toCharArray()[v.length() - 1] == 13)
                v = v.substring(0, v.length() - 1); // remove carriage returns at end in string

            if (v.chars().filter(c -> c == '.').count() > 1) {
                line.add(new ValueContainer<String>(v)); // multiple commas reinterpreted as string
                continue;
            }

            /* ------------------- PARSE DOUBLE ------------------- */
            // Double instance
            if (v.contains(".")) {
                line.add(new ValueContainer<Double>(Double.parseDouble(v)));
                continue;
            }

            /* ------------------- PARSE INTEGER ------------------- */
            // Integer instance
            long numbers = v.chars().filter(c -> c >= '0' && c <= '9').count();
            if (numbers == v.length()) {
                line.add(new ValueContainer<Integer>(Integer.parseInt(v)));
                continue;
            }

            line.add(new ValueContainer<String>(v)); // normal String value
        }
        //DEBUG
        //System.out.println(line.get(line.size() - 1).value);

        return line;
    }

    /**
     * Reads all lines into a table
     * Map(lineIndex, line)
     *
     * @return On success: true | On failure: false
     */
    private boolean parseMap() {
        this.table = new HashMap<>();
        for (int i = 0; i < this.totalLineCount; i++) {
            this.table.put(i, parseLine(i));
            if (this.C == CHECK.SYNTAX_ERROR) return false;
        }
        return true;
    }

    /**
     * Searches a value in given table and return it's first occurrence
     * RTI => return tuple index
     *
     * @param <T>   Value type
     * @param value The given value
     * @return On success: lineIndex at RTI 0 & value container index at RTI 1 | On failure: both -1
     */
    public final <T> int[] find(T value) {
        int[] result = {-1, -1};
        int index;
        for (Map.Entry<Integer, ArrayList<ValueContainer>> entry : this.table.entrySet()) {
            index = find(entry.getValue(), value);
            if (index != -1) {
                result[0] = entry.getKey();
                result[1] = index;
                break;
            }
        }
        return result;
    }

    /**
     * Searches a value in given line and return it's first occurrence
     *
     * @param <T>   Value type
     * @param line  Input line
     * @param value @{@link ValueContainer#value}
     * @return On success: index of line | On failure: -1
     */
    public final <T> int find(final ArrayList<ValueContainer> line, T value) {
        for (int i = 0; i < line.size(); i++)
            if (line.get(i).value.equals(value)) return i;
        return -1;
    }

    /**
     * Try to evaluate if value container is a int
     *
     * @param valueC @{@link ValueContainer}
     * @return If value container is of type int return itself | If not return 0 and CHECK is set to WRONG_TYPE
     */
    public final int tryInt(ValueContainer valueC) {
        if (valueC.value instanceof Integer) {
            this.C = CHECK.SUCCESS;
            return (int) valueC.value;
        } else {
            System.out.format(">> [CSVParser]: Can't cast %s to Integer. Check the enum var \"CHECK\"!\n", valueC.value);
            this.C = CHECK.WRONG_TYPE;
            return 0;
        }
    }

    /**
     * Try to evaluate if value container is a double
     *
     * @param valueC @{@link ValueContainer}
     * @return If value container is of type double return itself | If not return 0.0 and CHECK is set to WRONG_TYPE
     */
    public final Double tryDouble(ValueContainer valueC) {
        if (valueC.value instanceof Double) {
            this.C = CHECK.SUCCESS;
            return (double) valueC.value;
        } else {
            System.out.format(">> [CSVParser]: Can't cast %s to Double. Check the enum var \"CHECK\"!\n", valueC.value);
            this.C = CHECK.WRONG_TYPE;
            return 0.0;
        }
    }

    /**
     * Try to evaluate if value container is a string
     *
     * @param valueC @{@link ValueContainer}
     * @return If value container is of type string return itself | If not return null and CHECK is set to WRONG_TYPE
     */
    public final String tryString(ValueContainer valueC) {
        if (valueC.value instanceof String) {
            this.C = CHECK.SUCCESS;
            return (String) valueC.value;
        } else {
            System.out.format(">> [CSVParser]: Can't cast %s to String. Check the enum var \"CHECK\"!\n", valueC.value);
            this.C = CHECK.WRONG_TYPE;
            return null;
        }
    }

    /**
     * Getting the total line count
     *
     * @return @{@link CSVParser#totalLineCount}
     */
    public final long getTotalLineCount() {
        return this.totalLineCount;
    }

    /**
     * Getting the final table
     *
     * @return @{@link CSVParser#table}
     */
    public final HashMap<Integer, ArrayList<ValueContainer>> getTable() {
        return this.table;
    }

    /**
     * Getting a value container
     *
     * @param lIndex line index
     * @param index  value container index in line
     * @return @{@link ValueContainer}
     */
    public final ValueContainer getValueContainer(int lIndex, int index) {
        return this.table.get(lIndex).get(index);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (Map.Entry<Integer, ArrayList<ValueContainer>> entry : table.entrySet()) {
            result.append("{").append(entry.getKey()).append(":");
            for (ValueContainer vC : entry.getValue()) {
                ValueContainer lastEntry = entry.getValue().get(entry.getValue().size() - 1);
                result.append(vC != lastEntry ? vC.toString() + "," : vC.toString());
            }
            result.append("}\n");
        }
        return result.toString();
    }
}