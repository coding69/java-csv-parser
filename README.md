# Java CSV Parser
***Parse csv files like in your dreams***

### How to use?
Simply download or clone **src** folder in your working project.

### JavaDoc:
[Click here](https://javadoc-csvparser.web.app/)

### Build rules:
> This lib does not support embedded line breaks!
> + _,_ will be interpreted as separation (use USA/UK comma style)
> + _._ represents floating point values
> + _""_ are always string literals
> + Spaces will be ignored (if not marked)
> + Linebreaks will be ignored and make sure that there are no values laying on multiple lines
> + CSV files has to end with no character or a comma
> + The builder will overwrite csv files if it's path is set to already existing file

| rule                       |      example       |   value1    |  value2  | value3 |
|----------------------------|:------------------:|:-----------:|:--------:|:------:|
| Normal Values:             |       v1,v2        |     v1      |    v2    |   /    |
| USA/UK Comma Values:       |       0.1,10       |     0.1     |    10    |   /    |
| USA/UK Multiple Commas:    |      0.1.1,10      |   "0.1.1"   |    10    |   /    |
| Adjacent Commas (Illegal): |      1,,5.0,       |      /      |    /     |   /    |
| Quoted Values:             |      Name,Age      |   "Name"    |  "Age"   |   /    |
| Multiple Line Values:      |     Na\nme,Age     |     Na      |    me    |  Age   |
| Meaningfull Spaces:        | " Name Age",Gender | " Name Age" | "Gender" |   /    |
